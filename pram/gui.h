#include <pram/renderer.h>
#include <pram/ui.h>

#define FONT_SIZE 12
#define MAX_VERTEX_COUNT (4 * 1024)
#define MAX_INDEX_COUNT (6 * 1024)

#define add(a, b) _Generic((a), v2: v2_add)(a, b)
#define sub(a, b) _Generic((a), v2: v2_sub)(a, b)
#define mul(a, b) _Generic((a), v2: v2_mul)(a, b)
#define div(a, b) _Generic((a), v2: v2_div)(a, b)
#define divf(a, b) _Generic((a), v2: v2_divf)(a, b)
#define mulf(a, b) _Generic((a), v2: v2_mulf)(a, b)

struct glyph {
	i32 x, y;
	u32 width, height;
	i32 advance;
	i32 bearing_x;
	i32 bearing_y;
};

struct font {
	u32 texture;
	u32 width;
	u32 height;
	struct glyph glyphs[94];
	u32 kerning[94 * 94];
};
