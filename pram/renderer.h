struct vertex {
	v2 pos;
	v2 texcoord;
	v4 color;
	f32 texture;
};

struct render_context {
	struct vertex *vertices;
	u32 *indices;
	u32 *textures;

	u32 max_index_count;
	u32 index_count;
	u32 max_vertex_count;
	u32 vertex_count;
	u32 max_texture_count;
	u32 texture_count;

	i32 width;
	i32 height;
};

struct renderer {
	struct {
		i32 handle;
		i32 viewport;
		i32 textures[16];
	} shader;

	u32 vertex_array;
	u32 vertex_buffer;
	u32 index_buffer;
	u32 white_texture;
};

