static char *
readline(const char *prompt)
{
	fputs(prompt, stdout);
	fflush(stdout);

	char *line = NULL;
	size_t size = 0;
	ssize_t len;
	if ((len = getline(&line, &size, stdin)) != -1) {
		/* NOTE: remove the newline at the end of the string */
		line[len - 1] = '\0';
		return line;
	} else {
		if (line) {
			free(line);
		}

		return NULL;
	}
}

/*
 * NOTE: only checks if the bytes of the substring match the string, but not
 * if the string and the substring are of the same length
 */
static bool
string_equals(char *string, u32 string_length, char *substring)
{
	while (*substring && string_length-- > 0) {
		if (*string++ != *substring++) {
			return false;
		}
	}

	return string_length == 0;
}

static bool
string_split(char **string, char **out_start, u32 *out_length)
{
	char *at = *string;
	u32 length = 0;

	while (isspace(at[0])) {
		at++;
	}

	*out_start = at;

	while (at[0] && !isspace(at[0])) {
		length++;
		at++;
	}

	*out_length = length;
	*string = at;
	return at[0] != '\0';
}

static void
print_memory(struct pram_memory *memory)
{
	u32 input_count = memory->input_count;
	u32 register_count = memory->register_count;
	u32 count = MAX(input_count, register_count);

	printf("x    ");
	for (int i = 0; i < count; i++) {
		printf("%4d ", i);
	}
	putchar('\n');

	printf("I[x] ");
	i32 *input = memory->inputs;
	while (input_count-- > 0) {
		printf("%4d ", *input++);
	}
	putchar('\n');

	printf("R[x] ");
	i32 *reg = memory->registers;
	while (register_count-- > 0) {
		printf("%4d ", *reg++);
	}
	putchar('\n');
}

static void
print_expression(struct pram_expression *expr)
{
	switch (expr->type) {
	case PRAM_EXPR_NONE:
		break;
	case PRAM_EXPR_MACHINE_COUNT:
		printf("n");
		break;
	case PRAM_EXPR_MACHINE_INDEX:
		printf("i");
		break;
	case PRAM_EXPR_VARIABLE:
		printf("%s", expr->variable);
		break;
	case PRAM_EXPR_NUMBER:
		printf("%d", expr->number);
		break;
	case PRAM_EXPR_ADD:
		printf("(");
		print_expression(expr->lhs);
		printf(" + ");
		print_expression(expr->rhs);
		printf(")");
		break;
	case PRAM_EXPR_MUL:
		printf("(");
		print_expression(expr->lhs);
		printf(" * ");
		print_expression(expr->rhs);
		printf(")");
		break;
	}
}

static void
print_program(struct pram_program *program)
{
	struct pram_instruction *instruction = program->instructions;
	u32 instruction_count = program->instruction_count;

	for (u32 i = 0; i < instruction_count; i++) {
		printf("%4d: %s ", i, token_name[instruction->opcode]);
		print_expression(&instruction->arg);
		putchar('\n');

		instruction++;
	}
}

static int
cli_main(int argc, char *argv[])
{
	struct pram_program program = {0};
	struct pram_memory memory = {0};
	struct parser parser = {0};

	if (argc < 2) {
		printf("USAGE: pram [options] file\n\n"
			"OPTIONS\n"
			"    -f FILE load initial inputs from FILE\n"
			"    -i COUNT specify the number of input registers\n"
			"    -r COUNT specify the number of working registers\n"
			"COMMANDS\n"
			"    step    Execute one instruction per machine\n"
			"    finish  Execute program until all machines terminate\n"
			"    reset   Reset the registers and program counters\n"
			"    exit    Exit the program\n");
		return 0;
	}

	argv++;
	argc--;

	/* default values */
	memory.input_count = 32;

	char *code_path = NULL;
	char *input_path = NULL;
	while (argc-- > 0) {
		char *at = *argv++;

		if (at[0] == '-' && at[1] != '\0') {
			switch (at[1]) {
			case 'f':
				input_path = *argv++;
				argc--;
				break;
			case 'i':
				memory.input_count = strtoul(*argv++, NULL, 10);
				argc--;
				break;
			case 'r':
				memory.register_count = strtoul(*argv++, NULL, 10);
				argc--;
				break;
			case 'm':
				program.machine_count = strtoul(*argv++, NULL, 10);
				argc--;
				break;
			}
		} else {
			code_path = at;
		}
	}

	if (memory.input_count == 0) {
		fprintf(stderr, "Invalid number of inputs\n");
		return 1;
	}

	memory_init(&memory, input_path);
	if (program.machine_count == 0) {
		program.machine_count = 1;
	}

	if (memory.register_count < program.machine_count) {
		fprintf(stderr, "Invalid number of registers: %d < %d\n",
			memory.register_count, program.machine_count);
		goto error_invalid_register_count;
	}

	program.counters = ecalloc(program.machine_count, sizeof(*program.counters));

	/* read in the assembler code */
	if (!code_path) {
		printf("Reading code from stdin...\n");
	}

	if (!buffer_read(&parser.buffer, code_path)) {
		fprintf(stderr, "Failed to read file\n");
		goto error_read;
	}

	/* parse the instructions from the code */
	if (!parse(&parser, &program)) {
		fprintf(stderr, "Failed to parse the program\n");
		goto error_parse;
	}

	print_program(&program);
	print_memory(&memory);

	/* use the filename as the prompt */
	char prompt[512];
	char *line = 0;
	char *next_line = 0;
	snprintf(prompt, sizeof(prompt), "%s> ", code_path);

	u32 global_counter = 0;
	while ((next_line = readline(prompt))) {
		/* if an empty command was given then use the previous command */
		if (strlen(next_line) != 0) {
			if (line) {
				free(line);
			}

			line = next_line;
		} else {
			free(next_line);
		}

		char *iter = line;
		char *command = 0;
		u32 command_length = 0;
		string_split(&iter, &command, &command_length);

		if (string_equals(command, command_length, "step")) {
			/* print the executed instructions of each machine */
			u32 machine_count = program.machine_count;
			for (u32 i = 0; i < machine_count; i++) {
				u32 counter = program.counters[i];
				if (counter < program.instruction_count) {
					struct pram_instruction *instruction =
						&program.instructions[counter];

					i32 x = expression_eval(&instruction->arg, i, machine_count);
					const char *name = token_name[instruction->opcode];

					printf("%2d: %5s %d\n", i, name, x);
				}
			}

			printf("\n");

			if (!program_step(&program, &memory)) {
				printf("Program terminated\n");
			} else {
				global_counter++;
				snprintf(prompt, sizeof(prompt), "%s:%d> ", code_path,
					global_counter);
			}

			print_memory(&memory);
		} else if (string_equals(command, command_length, "finish")) {
			while (program_step(&program, &memory)) {}
		} else if (string_equals(command, command_length, "reset")) {
			usz size = program.machine_count * sizeof(*program.counters);
			memset(program.counters, 0, size);
			memory_init(&memory, input_path);
			print_memory(&memory);
		} else if (string_equals(command, command_length, "exit") ||
				string_equals(command, command_length, "quit")) {
			break;
		} else if (string_equals(command, command_length, "help")) {
			printf("\nCOMMANDS\n"
				"- step    Execute one instruction per machine\n"
				"- finish  Execute program until all machines terminate\n"
				"- reset   Reset the registers and program counters\n"
				"- set     Set a value of a register\n"
				"- seti    Set a value of an input register\n"
				"- exit    Exit the program\n\n");
		} else if (string_equals(command, command_length, "seti")) {
			char *endptr = 0;
			u32 input = strtoul(iter, &endptr, 10);
			i32 value = 0;
			if (iter != endptr) {
				iter = endptr;
				value = strtol(iter, &endptr, 10);
			}

			if (iter != endptr && input < memory.input_count) {
				printf("R[%u] = %d\n", input, value);
				memory.inputs[input] = value;
				print_memory(&memory);
			} else {
				printf("Invalid arguments\n");
			}
		} else if (string_equals(command, command_length, "set")) {
			char *endptr = 0;
			u32 reg = strtoul(iter, &endptr, 10);
			i32 value = 0;
			if (iter != endptr) {
				iter = endptr;
				value = strtol(iter, &endptr, 10);
			}

			if (iter != endptr && reg < memory.register_count) {
				printf("R[%u] = %d\n", reg, value);
				memory.registers[reg] = value;
				print_memory(&memory);
			} else {
				printf("Invalid arguments\n");
			}
		} else {
			printf("Unrecognized command\n");
		}
	}

	if (line) {
		free(line);
	}

	printf("exiting...\n");
	free(parser.buffer.data);
	program_finish(&program);
	memory_finish(&memory);

	return 0;
error_parse:
	free(parser.buffer.data);
error_read:
	free(program.counters);
	free(memory.registers);
error_invalid_register_count:
	free(memory.inputs);
	return 1;
}
