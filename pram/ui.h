enum ui_unit {
	UI_AUTO = 0,
	UI_PERCENT,
	UI_PX,
	UI_EM,
};

struct ui_size {
	u32 unit;
	f32 value;
	f32 strictness;
};

enum ui_element_flags {
	UI_SELECTABLE       = 1 <<  0,
	UI_DRAW_TEXT        = 1 <<  1,
	UI_DRAW_BACKGROUND  = 1 <<  2,
	UI_DRAW_BACKGROUND2 = 1 <<  3,
	UI_DRAW_NUMBER      = 1 <<  4,
	UI_LAYOUT_X         = 1 <<  5,
	UI_SCROLL_X         = 1 <<  6,
	UI_SCROLL_Y         = 1 <<  7,
	UI_FLOAT_X          = 1 <<  8,
	UI_FLOAT_Y          = 1 <<  9,
	UI_CENTER_TEXT      = 1 << 10,
};

struct ui_element {
	u32 flags;
	u32 id;
	struct ui_size size[2];

	union {
		char *text;
		f32 number;
	};

	v2 scroll;
	struct rect rect;
	struct rect clipped_rect;

	// NOTE: linked list of children
	struct ui_element *head;
	struct ui_element *tail;
	struct ui_element *parent;
	struct ui_element *next;
	struct ui_element *prev;
};

struct ui_context {
	struct {
		struct {
			v2 pos;
			v2 scroll;
			u8 buttons[8];
		} mouse;

		struct string text;
	} input;

	struct ui_element *drop_downs[8];
	struct ui_element *tooltip;
	struct ui_element *root;
	struct ui_element *current_parent;
	struct ui_element *elements;
	u32 max_element_count;
	u32 element_count;
	u32 drop_down_count;
	u32 active;
	u32 hot;
	u32 hot_scroll;

	struct render_context *ctx;
	struct font *font;
};

#define ui_defer(begin, end) \
	for (int __ui_i = ((begin), 0); !__ui_i; __ui_i++, (end))

#define ui_row() ui_defer(ui_row_begin(), ui_end())
#define ui_menu_bar() ui_defer(ui_menu_bar_begin(), ui_end())
#define ui_tooltip() ui_defer(ui_tooltip_begin(), ui_tooltip_end())
#define ui_drop_down() ui_defer(ui_drop_down_begin(), ui_drop_down_end())
