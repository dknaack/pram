#include <glad/glad.h>
#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <pram/gui.h>

static inline v2 v2_add(v2 a, v2 b) { return v2(a.x + b.x, a.y + b.y); }
static inline v2 v2_sub(v2 a, v2 b) { return v2(a.x - b.x, a.y - b.y); }
static inline v2 v2_mul(v2 a, v2 b) { return v2(a.x * b.x, a.y * b.y); }
static inline v2 v2_div(v2 a, v2 b) { return v2(a.x / b.x, a.y / b.y); }
static inline v2 v2_divf(v2 a, f32 f) { return v2(a.x / f, a.y / f); }

static inline bool
was_pressed(u8 button)
{
	return (button & 3) == 1;
}

static inline bool
is_down(u8 button)
{
	return (button & 1) != 0;
}

static inline bool
was_released(u8 button)
{
	return (button & 3) == 2;
}

static inline struct rect
rect_init(v2 pos, v2 size)
{
	struct rect result = {0};

	result.min = pos;
	result.max = add(pos, size);

	return result;
}

static inline struct rect
rect_inf(void)
{
	struct rect result;

	result.min.x = result.min.y = -F32_INF;
	result.max.x = result.max.y = F32_INF;

	return result;
}

static inline struct rect
rect_zero(void)
{
	struct rect result = {0};

	return result;
}

static inline void
rect_move(struct rect *rect, f32 x, f32 y)
{
	rect->min.x += x;
	rect->min.y += y;
	rect->max.x += x;
	rect->max.y += y;
}

static inline v2
rect_size(struct rect rect)
{
	v2 result = sub(rect.max, rect.min);

	return result;
}

static inline bool
rect_is_empty(struct rect rect)
{
	bool result = rect.min.x >= rect.max.x || rect.min.y >= rect.max.y;

	return result;
}

static inline bool
rect_contains(struct rect rect, v2 point)
{
	bool result = rect.min.x <= point.x && point.x < rect.max.x
		&& rect.min.y <= point.y && point.y < rect.max.y;

	return result;
}

static struct rect
rect_intersect(struct rect a, struct rect b)
{
	struct rect result;

	result.min.x = MAX(a.min.x, b.min.x);
	result.min.y = MAX(a.min.y, b.min.y);
	result.max.x = MIN(a.max.x, b.max.x);
	result.max.y = MIN(a.max.y, b.max.y);

	if (rect_is_empty(result)) {
		result.max = result.min;
	}

	return result;
}

static struct rect
rect_union(struct rect a, struct rect b)
{
	struct rect result;

	result.min.x = MIN(a.min.x, b.min.x);
	result.min.y = MIN(a.min.y, b.min.y);
	result.max.x = MAX(a.max.x, b.max.x);
	result.max.y = MAX(a.max.y, b.max.y);

	return result;
}

static struct string
read_file(char *filename)
{
	struct string result = {0};

	FILE *file = fopen(filename, "rb");
	if (file) {
		fseek(file, 0, SEEK_END);
		result.length = ftell(file);
		fseek(file, 0, SEEK_SET);

		result.at = calloc(result.length + 1, 1);
		if (result.at) {
			fread(result.at, result.length, 1, file);
			result.at[result.length] = '\0';
		}

		fclose(file);
	}

	return result;
}

static f32
lerp(f32 a, f32 b, f32 t)
{
	return t * b + (1 - t) * a;
}

#include "pram/renderer.c"
#include "pram/ui.c"

static void
glfw_character_callback(GLFWwindow *window, u32 codepoint)
{
	printf("%lc\n", codepoint);
}

static void
glfw_scroll_callback(GLFWwindow *window, f64 x, f64 y)
{
	struct ui_context *ui = glfwGetWindowUserPointer(window);
	ui->input.mouse.scroll = v2(x, y);
}

static bool
ui_menu_bar_button(char *label)
{
	bool result = false;

	u32 flags = UI_SELECTABLE | UI_DRAW_TEXT | UI_CENTER_TEXT | UI_DRAW_BACKGROUND;
	struct ui_element *element = ui_push_element(flags, label);
	element->text = label;

	return result;
}

#define ui_list_view(label, item_count, min, max) \
	ui_defer(ui_list_view_begin(label, item_count, min, max), ui_list_view_end())

static void
ui_list_view_begin(char *label, u32 item_count, u32 *min_item, u32 *max_item)
{
	struct ui_element *container = ui_push_container(0, label);
	container->size[0] = ui_percent(1, 0);

	struct ui_element *header = ui_push_element(UI_DRAW_BACKGROUND|UI_DRAW_TEXT, "header");
	header->text = label;
	header->size[0] = ui_percent(1, 1);

	u32 items_flags = UI_LAYOUT_X | UI_SCROLL_Y;
	struct ui_element *body = ui_push_container(items_flags, "body");
	body->size[0] = ui_percent(1, 1);
	body->size[1] = ui_em(20);

	ui_spacer_x(ui_px(8));

	struct ui_element *items = ui_push_container(0, "items");
	items->size[0] = ui_percent(1, 0);
	items->size[1] = ui_em(item_count);
	assert(items->parent == body);

	*min_item = 0;
	*max_item = item_count - 1;
}

static void
ui_list_view_end(void)
{
	ui_end();
	ui_end();
	ui_end();
}

static void
ui_list_item(u32 key, char *item)
{
	u32 flags = UI_DRAW_TEXT;
	struct ui_element *element = ui_push_element(flags, item);
	element->size[0] = ui_percent(1, 0);
	element->size[1] = ui_em(1);
	element->text = item;
}

static int
gui_main(int argc, char *argv[])
{
	struct ui_context ui = {0};
	int result = 1;

	char *source_path = NULL;
	for (u32 i = 1; i < argc; i++) {
		if (argv[i][0] != '-') {
			source_path = argv[i];
		}
	}

	if (!source_path) {
		fprintf(stderr, "Missing source code path\n");
		goto error_source_code;
	}

	struct parser parser = {0};
	if (!buffer_read(&parser.buffer, source_path)) {
		perror("Failed to read source code");
		goto error_source_code;
	}

	struct pram_program program = {0};
	if (!parse(&parser, &program)) {
		fprintf(stderr, "Failed to parse the program\n");
		goto error_parse;
	}

	// NOTE: initialize the memory and program counters
	u32 input_count = 32;
	u32 machine_count = 16;
	u32 register_count = 32;

	struct pram_memory memory = {0};
	memory.input_count = input_count;
	memory.register_count = register_count;
	memory_init(&memory, NULL);

	program.machine_count = machine_count;
	program.counters = ecalloc(program.machine_count, sizeof(*program.counters));

	// NOTE: initialize the window
	if (!glfwInit()) {
		goto error_glfw_init;
	}

	GLFWwindow *window = glfwCreateWindow(640, 480, "test", NULL, NULL);
	if (!window) {
		goto error_glfw_create_window;
	}

	glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_API);
	glfwWindowHint(GLFW_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_VERSION_MINOR, 3);
	glfwMakeContextCurrent(window);

	glfwSetWindowUserPointer(window, &ui);
	glfwSetCharCallback(window, glfw_character_callback);
	glfwSetScrollCallback(window, glfw_scroll_callback);

	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
		goto error_glad;
	}

	// NOTE: initialize the renderer
	struct renderer renderer = {0};
	renderer_init(&renderer);

	// NOTE: initialize the ui
	struct font font = load_font("assets/font_mono.bin");
	ui.font = &font;
	ui.max_element_count = 1024;
	ui.elements = calloc(ui.max_element_count, sizeof(*ui.elements));
	assert(ui.elements);

	bool terminated = false;
	char string_pool[4096] = {0};

	while (!glfwWindowShouldClose(window)) {
		struct render_context *ctx = render_context_create(4 << 12, 6 << 12, 16);

		// get the window size
		glfwGetFramebufferSize(window, &ctx->width, &ctx->height);

		// get the mouse position
		f64 cursor_x, cursor_y;
		glfwGetCursorPos(window, &cursor_x, &cursor_y);
		ui.input.mouse.pos = v2(cursor_x, ctx->height - cursor_y);

		// get the mouse button state
		i32 state = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT);
		u8 is_down = state == GLFW_PRESS;
		ui.input.mouse.buttons[1] = (ui.input.mouse.buttons[1] << 1) | is_down;

		// NOTE: begin of frame
		ui_begin_frame(&ui, ctx);

		ui_menu_bar() {
			if (ui_menu_bar_button("File")) {
				ui_drop_down() {
					ui_button("Open");
					ui_button("Save");
					ui_button("Save As");
				}
			}

			if (ui_menu_bar_button("Edit")) {
				ui_drop_down() {
					ui_button("Undo");
					ui_button("Redo");
					ui_button("Cut");
					ui_button("Copy");
					ui_button("Paste");
				}
			}

			ui_menu_bar_button("View");
			ui_menu_bar_button("Help");

			ui_spacer_x(ui_percent(1, 0));
		}

		ui_separator();

		{
			struct ui_element *row = ui_push_container(UI_LAYOUT_X, "row");
			row->size[0] = ui_percent(1.0f, 0);
			row->size[1] = ui_percent(1.0f, 0);

			// NOTE: source code panel
			{
				u32 panel_flags = UI_DRAW_BACKGROUND | UI_SCROLL_Y;
				struct ui_element *panel = ui_push_container(panel_flags, "source-panel");
				panel->size[0] = ui_percent(0.7f, 0);
				panel->size[1] = ui_percent(1.0f, 0);

				struct ui_element *text = ui_push_element(UI_DRAW_TEXT, "content");
				text->text = parser.buffer.data;
				ui_end();
			}

			ui_separator();

			// NOTE: register panel
			{
				struct ui_element *panel = ui_push_container(UI_DRAW_BACKGROUND, "register-panel");
				panel->size[0] = ui_percent(0.3f, 0);
				panel->size[1] = ui_percent(1.0f, 0);

				if (ui_button("Reset")) {
					usz size = program.machine_count * sizeof(*program.counters);
					memset(program.counters, 0, size);
					memory_init(&memory, NULL);
					terminated = false;
				}

				if (!terminated) {
					if (ui_button("Step")) {
						terminated = !program_step(&program, &memory);
					}

					if (ui_button("Finish")) {
						while (!terminated) {
							terminated = !program_step(&program, &memory);
						}
					}
				}

				char *string = string_pool;
				u32 min, max;
				ui_separator();

				if (terminated) {
					ui_label("Status: TERMINATED!");
				} else {
					ui_label("Status: Running...");
				}

				ui_separator();
				ui_list_view("Inputs", memory.input_count, &min, &max) {
					for (u32 i = min; i <= max; i++) {
						snprintf(string, 32, "%2d: %4d", i, memory.inputs[i]);
						ui_list_item(i, string);
						string += 32;

						assert(string - string_pool < sizeof(string_pool));
					}
				}

				ui_separator();
				ui_list_view("Registers", memory.register_count, &min, &max) {
					for (u32 i = min; i <= max; i++) {
						snprintf(string, 32, "%2d: %4d", i, memory.registers[i]);
						ui_list_item(i, string);
						string += 32;

						assert(string - string_pool < sizeof(string_pool));
					}
				}

				ui_separator();
				ui_end();
			}

			ui_end();
		}

		ui_end_frame(&ui);
		ui.input.mouse.scroll = v2(0, 0);

		// NOTE: end of frame
		renderer_submit(&renderer, ctx);
		render_context_destroy(ctx);
		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	free(ui.elements);
	renderer_finish(&renderer);
	result = 0;

error_glad:
	glfwDestroyWindow(window);
error_glfw_create_window:
	glfwTerminate();
error_glfw_init:
	program_finish(&program);
	memory_finish(&memory);
error_parse:
	free(parser.buffer.data);
error_source_code:
	return result;
}
