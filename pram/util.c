static void
die(const char *msg)
{
	perror(msg);
	exit(1);
}

static void *
ecalloc(usz nmemb, usz size)
{
	void *ptr = calloc(nmemb, size);

	if (!ptr) {
		die("calloc");
	}

	return ptr;
}

static bool
buffer_read(struct buffer *buffer, const char *path)
{
	FILE *f = stdin;

	if (path && !(f = fopen(path, "r"))) {
		return false;
	}

	u32 size = 2 * BUFSIZ;
	u32 len = 0;
	usz s;
	char *data = ecalloc(size, 1);

	while ((s = fread(data + len, 1, BUFSIZ, f))) {
		len += s;
		if(BUFSIZ + len + 1 > size) {
			size *= 2;
			if (!(data = realloc(data, size))) {
				die("realloc");
			}
		}
	}

	data[len] = '\0';
	buffer->data = data;
	buffer->size = len;
	buffer->start = 0;

	if (path) {
		fclose(f);
	}

	return true;
}
