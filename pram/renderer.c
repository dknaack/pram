#define consume(buffer, type) ((type *)consume_size(buffer, sizeof(type)))
#define consume_array(buffer, count, type) ((type *)consume_size(buffer, (count) * sizeof(type)))

static void *
consume_size(struct string *buffer, usz size)
{
	void *result = NULL;

	if (buffer->length >= size) {
		result = (void *)buffer->at;
		buffer->at += size;
		buffer->length -= size;
	}

	return result;
}

static struct font
load_font(char *filename)
{
	struct font font = {0};

	struct string file = read_file(filename);
	if (file.at) {
		struct string buffer = file;

		font.width = *consume(&buffer, u32);
		font.height = *consume(&buffer, u32);

		struct glyph *glyphs = consume_array(&buffer, 94, struct glyph);
		u32 *kerning = consume_array(&buffer, 94 * 94, u32);
		assert(glyphs && kerning);

		memcpy(&font.glyphs, glyphs, sizeof(font.glyphs));
		memcpy(&font.kerning, kerning, sizeof(font.kerning));

		glGenTextures(1, &font.texture);
		glBindTexture(GL_TEXTURE_2D, font.texture);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		u8 *pixels = (u8 *)buffer.at;
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, font.width, font.height,
			0, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
		glGenerateMipmap(GL_TEXTURE_2D);
	}

	return font;
}

static GLint
gl_shader_create(GLenum type, char *source)
{
	char *strings[2] = {0};

	strings[0] =
		"#version 330 core\n"
		"#define v2 vec2\n"
		"#define v3 vec3\n"
		"#define v4 vec4\n"
		"#define m4x4 mat4\n"
		"#define m3x3 mat3\n"
		"#define f32 float\n"
		"#define i32 int\n"
		"#define u32 uint\n";
	strings[1] = source;

	GLint shader = glCreateShader(type);
	if (shader) {
		glShaderSource(shader, 2, (const char **)strings, NULL);
		glCompileShader(shader);

		int compiled = false;
		glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
		if (!compiled) {
			char info_log[1024] = {0};
			glGetShaderInfoLog(shader, sizeof(info_log), NULL, info_log);
			fprintf(stderr, "%s\n", info_log);
			assert(false);
		}
	}

	return shader;
}

static GLint
gl_program_create(char *vs_src, char *fs_src)
{
	GLint program = glCreateProgram();
	if (program) {
		GLint vertex_shader = gl_shader_create(GL_VERTEX_SHADER, vs_src);
		GLint fragment_shader = gl_shader_create(GL_FRAGMENT_SHADER, fs_src);

		glAttachShader(program, vertex_shader);
		glAttachShader(program, fragment_shader);
		glBindAttribLocation(program, 0, "vert_pos");
		glLinkProgram(program);

		glDeleteShader(vertex_shader);
		glDeleteShader(fragment_shader);

		int linked = false;
		glGetProgramiv(program, GL_LINK_STATUS, &linked);
		if (!linked) {
			char info_log[1024] = {0};
			glGetProgramInfoLog(program, sizeof(info_log), NULL, info_log);
			fprintf(stderr, "Failed to create program: %s\n", info_log);
			program = 0;
		}
	}

	return program;
}

static void
gl_texture_bind(u32 index, u32 texture)
{
	glActiveTexture(GL_TEXTURE0 + index);
	glBindTexture(GL_TEXTURE_2D, texture);
}

static void
renderer_init(struct renderer *renderer)
{
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	// NOTE: initialize the shader program
	struct string vs_src = read_file("assets/vertex_shader.glsl");
	struct string fs_src = read_file("assets/fragment_shader.glsl");
	assert(vs_src.at && fs_src.at);

	renderer->shader.handle = gl_program_create(vs_src.at, fs_src.at);
	renderer->shader.viewport = glGetUniformLocation(renderer->shader.handle, "viewport");
	u32 color_textures = glGetUniformLocation(renderer->shader.handle, "color_textures");
	i32 samplers[16] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 };
	glUseProgram(renderer->shader.handle);
	glUniform1iv(color_textures, LENGTH(samplers), samplers);

	// NOTE: initialize a white texture
	glGenTextures(1, &renderer->white_texture);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, renderer->white_texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);

	f32 white[] = { 1, 1, 1, 1 };
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 1, 1, 0, GL_RGBA, GL_FLOAT, white);
	glGenerateMipmap(GL_TEXTURE_2D);

	// NOTE: initialize the vertex array
	glGenVertexArrays(1, &renderer->vertex_array);
	glBindVertexArray(renderer->vertex_array);

	glGenBuffers(1, &renderer->vertex_buffer);
	glBindBuffer(GL_ARRAY_BUFFER, renderer->vertex_buffer);
	glBufferData(GL_ARRAY_BUFFER, MAX_VERTEX_COUNT * sizeof(struct vertex),
		NULL, GL_STREAM_DRAW);

	glGenBuffers(1, &renderer->index_buffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, renderer->index_buffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, MAX_INDEX_COUNT * sizeof(u32),
		NULL, GL_STREAM_DRAW);

	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE,
		sizeof(struct vertex), (void *)offsetof(struct vertex, pos));
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE,
		sizeof(struct vertex), (void *)offsetof(struct vertex, texcoord));
	glVertexAttribPointer(2, 4, GL_FLOAT, GL_FALSE,
		sizeof(struct vertex), (void *)offsetof(struct vertex, color));
	glVertexAttribPointer(3, 1, GL_FLOAT, GL_FALSE,
		sizeof(struct vertex), (void *)offsetof(struct vertex, texture));

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	glEnableVertexAttribArray(2);
	glEnableVertexAttribArray(3);
	assert(glGetError() == 0);
}

static void
renderer_finish(struct renderer *renderer)
{
	glDeleteVertexArrays(1, &renderer->vertex_array);
	glDeleteBuffers(1, &renderer->vertex_buffer);
	glDeleteBuffers(1, &renderer->index_buffer);
	glDeleteProgram(renderer->shader.handle);
	glDeleteTextures(1, &renderer->white_texture);
}

static void
renderer_submit(struct renderer *renderer, struct render_context *ctx)
{
	u32 width = ctx->width;
	u32 height = ctx->height;

	// NOTE: update the vertex and index buffers
	glBindVertexArray(renderer->vertex_array);
	glBindBuffer(GL_ARRAY_BUFFER, renderer->vertex_buffer);
	glBufferSubData(GL_ARRAY_BUFFER, 0,
		ctx->vertex_count * sizeof(struct vertex), ctx->vertices);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, renderer->index_buffer);
	glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0,
		ctx->index_count * sizeof(u32), ctx->indices);

	glViewport(0, 0, width, height);
	glClearColor(0.f, 0.f, 0.f, 1.f);
	glClear(GL_COLOR_BUFFER_BIT);

	glUseProgram(renderer->shader.handle);
	glUniform2f(renderer->shader.viewport, ctx->width, ctx->height);

	gl_texture_bind(0, renderer->white_texture);
	for (u32 i = 1; i < ctx->texture_count; i++) {
		gl_texture_bind(i, ctx->textures[i]);
		assert(ctx->textures[i] != 0);
	}

	glDrawElements(GL_TRIANGLES, ctx->index_count, GL_UNSIGNED_INT, NULL);
}

static void
render_quad(struct render_context *ctx, struct rect rect, v4 color, u32 texture, struct rect uv)
{
	assert(ctx->vertex_count + 4 <= ctx->max_vertex_count);
	assert(ctx->index_count + 6 <= ctx->max_index_count);

	u32 texture_index = 0;
	if (texture != 0) {
		for (u32 i = 1; i < ctx->texture_count; i++) {
			if (ctx->textures[i] == texture) {
				texture_index = i;
			}
		}

		if (texture_index == 0) {
			assert(ctx->texture_count + 1 <= ctx->max_texture_count);
			texture_index = ctx->texture_count++;
			ctx->textures[texture_index] = texture;
		}
	}

	struct vertex *vertex = ctx->vertices + ctx->vertex_count;

	vertex[0].pos = v2(rect.min.x, rect.min.y);
	vertex[1].pos = v2(rect.max.x, rect.min.y);
	vertex[2].pos = v2(rect.min.x, rect.max.y);
	vertex[3].pos = v2(rect.max.x, rect.max.y);

	vertex[0].texcoord = v2(uv.min.x, uv.max.y);
	vertex[1].texcoord = v2(uv.max.x, uv.max.y);
	vertex[2].texcoord = v2(uv.min.x, uv.min.y);
	vertex[3].texcoord = v2(uv.max.x, uv.min.y);

	vertex[0].color = color;
	vertex[1].color = color;
	vertex[2].color = color;
	vertex[3].color = color;

	vertex[0].texture = texture_index;
	vertex[1].texture = texture_index;
	vertex[2].texture = texture_index;
	vertex[3].texture = texture_index;

	u32 *index = ctx->indices + ctx->index_count;
	u32 last_vertex = ctx->vertex_count;

	*index++ = last_vertex + 0;
	*index++ = last_vertex + 1;
	*index++ = last_vertex + 2;

	*index++ = last_vertex + 1;
	*index++ = last_vertex + 3;
	*index++ = last_vertex + 2;

	ctx->index_count += 6;
	ctx->vertex_count += 4;
}

static void
render_rect(struct render_context *ctx, struct rect rect, v4 color)
{
	render_quad(ctx, rect, color, 0, rect_init(v2(0, 0), v2(1, 1)));
}

static struct rect
render_text(struct render_context *ctx, v2 pos, struct rect text_bounds,
		char *text, struct font *font, v4 color)
{
	struct rect bounds;
	bounds.min = bounds.max = pos;

	f32 font_scale = 1.f;
	f32 font_width = font->width;
	f32 font_height = font->height;

	v2 start = pos;

	u32 prev_codepoint = 0;
	while (*text) {
		u32 codepoint = *text++;

		// NOTE: anything outside ascii is not supported.
		if ('!' <= codepoint && codepoint <= '~') {
			if ('!' <= prev_codepoint && prev_codepoint <= '~') {
				u32 kerning_index = (prev_codepoint - '!') * 94 + (codepoint - '!');
				f32 kerning = font->kerning[kerning_index];
				pos.x += 2.f * kerning;
			}

			u32 glyph_index = codepoint - '!';
			struct glyph *glyph = &font->glyphs[glyph_index];

			f32 width = glyph->width * font_scale;
			f32 height = glyph->height * font_scale;
			f32 bearing_y = glyph->bearing_y * font_scale;
			f32 bearing_x = glyph->bearing_x * font_scale;
			v2 glyph_pos = add(pos, v2(bearing_x, bearing_y - height));

			struct rect glyph_bounds = rect_init(glyph_pos, v2(width, height));
			glyph_bounds = rect_intersect(glyph_bounds, text_bounds);
			if (!rect_is_empty(glyph_bounds)) {
				bounds = rect_union(bounds, glyph_bounds);

				if (ctx) {
					struct rect uv;
					uv.min = divf(sub(glyph_bounds.min, glyph_pos), font_scale);
					uv.max = divf(sub(glyph_bounds.max, glyph_pos), font_scale);

					uv.min.x = (uv.min.x + glyph->x) / font_width;
					uv.min.y = (uv.min.y + glyph->y) / font_height;
					uv.max.x = (uv.max.x + glyph->x) / font_width;
					uv.max.y = (uv.max.y + glyph->y) / font_height;

					render_quad(ctx, glyph_bounds, color, font->texture, uv);
				}

				pos.x += glyph->advance * font_scale;
			}

			prev_codepoint = codepoint;
		} else if (codepoint == ' ') {
			pos.x += 0.75f * FONT_SIZE * font_scale;
		} else if (codepoint == '\t') {
			pos.x += 4 * 0.75f * FONT_SIZE * font_scale;
		} else if (codepoint == '\n') {
			pos.y -= 1.25f * FONT_SIZE;
			pos.x = start.x;
		} else {
			pos.x += font_scale;
			prev_codepoint = 0;
		}
	}

	// NOTE: include the first line in the bounds
	start.y -= 1.25f * FONT_SIZE;
	return bounds;
}

static struct render_context *
render_context_create(u32 max_index_count, u32 max_vertex_count, u32 max_texture_count)
{
	struct render_context *ctx = calloc(1, sizeof(*ctx));

	if (ctx) {
		ctx->max_index_count = max_index_count;
		ctx->max_vertex_count = max_vertex_count;
		ctx->max_texture_count = max_texture_count;

		ctx->indices = calloc(max_index_count, sizeof(*ctx->indices));
		ctx->vertices = calloc(max_vertex_count, sizeof(*ctx->vertices));
		ctx->textures = calloc(max_texture_count, sizeof(*ctx->textures));

		// NOTE: first texture is the white texture
		ctx->texture_count = 1;
	}

	return ctx;
}

static void
render_context_destroy(struct render_context *ctx)
{
	free(ctx->indices);
	free(ctx->vertices);
	free(ctx->textures);
	free(ctx);
}
