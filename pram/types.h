#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

typedef uintptr_t usz;
typedef uint64_t  u64;
typedef uint32_t  u32;
typedef uint16_t  u16;
typedef uint8_t   u8;

typedef intptr_t isz;
typedef int64_t  i64;
typedef int32_t  i32;
typedef int16_t  i16;
typedef int8_t   i8;

typedef double f64;
typedef float  f32;

typedef union {
	struct { f32 x, y; };
	f32 e[2];
} v2;

typedef	union {
	struct { f32 x, y, z; };
	struct { f32 r, g, b; };
	f32 e[3];
} v3;

typedef union {
	struct { f32 x, y, z, w; };
	f32 e[4];
} v4;

struct string {
	char *at;
	usz length;
};

struct buffer {
	char *data;
	usz size;
	usz start;
};

struct rect {
	v2 min;
	v2 max;
};

#define v2(x, y)       (v2){{ (x), (y) }}
#define v3(x, y, z)    (v3){{ (x), (y), (z) }}
#define v4(x, y, z, w) (v4){{ (x), (y), (z), (w) }}

#define assert(expr) ((expr) ? (void)0 : __builtin_trap())

#define BETWEEN(x, min, max) ((min) <= (x) && (x) < (max))
#define LENGTH(x) (sizeof(x)/sizeof(*(x)))
#define MAX(a, b) ((a) > (b)? (a) : (b))
#define MIN(a, b) ((a) < (b)? (a) : (b))
#define CLAMP(x, a, b) MAX((a), MIN((b), (x)))
#define CLAMP01(x) CLAMP((x), 0, 1)
#define STRINGIFY_(expr) #expr
#define STRINGIFY(expr) STRINGIFY_(expr)

#define F32_INF (1.0f/0.0f)
