#include <stdarg.h>

static struct ui_context *ui;
static f32 ui_line_height = 20.0f;

static struct ui_size
ui_percent(f32 value, f32 strictness)
{
	struct ui_size result;

	result.unit = UI_PERCENT;
	result.value = value;
	result.strictness = strictness;

	return result;
}

static struct ui_size
ui_px(f32 value)
{
	struct ui_size result;
	result.unit = UI_PX;
	result.value = value;
	result.strictness = 1.0f;

	return result;
}

static struct ui_size
ui_em(f32 value)
{
	struct ui_size result;
	result.unit = UI_EM;
	result.value = value;
	result.strictness = 1.0f;

	return result;
}

static struct ui_size
ui_auto(void)
{
	struct ui_size result;
	result.unit = UI_AUTO;
	result.strictness = 1.0f;

	return result;
}

static u32
ui_hash(u32 hash, char *byte)
{
	while (*byte) {
		hash = (hash << 5) + hash + *byte++;
	}

	return hash;
}

// TODO: use a hash map, so that the rect field for each element
// is the correct one at each call site.
static struct ui_element *
ui_push_element(u32 flags, char *string)
{
	struct ui_element *parent = ui->current_parent;
	u32 id = ui_hash(parent->id, string);

	struct ui_element *element = &ui->elements[ui->element_count++];
	element->flags = flags;
	element->id = id;
	element->parent = ui->current_parent;
	element->size[0] = element->size[1] = ui_auto();
	element->prev = element->next = NULL;
	element->head = element->tail = NULL;

	struct ui_element *tail = element->parent->tail;
	if (tail) {
		tail->next = element;
		element->prev = tail;
		element->parent->tail = element;
	} else {
		element->parent->head = element->parent->tail = element;
	}

	return element;
}

static struct ui_element *
ui_push_elementf(u32 flags, char *fmt, ...)
{
	char tmp[1024];
	va_list ap;

	va_start(ap, fmt);
	vsnprintf(tmp, sizeof(tmp), fmt, ap);
	va_end(ap);

	return ui_push_element(flags, tmp);
}

static struct ui_element *
ui_push_container(u32 flags, char *string)
{
	return ui->current_parent = ui_push_element(flags, string);
}

static void
ui_begin_frame(struct ui_context *_ui, struct render_context *ctx)
{
	ui = _ui;
	assert(ui && ctx);

	// NOTE: initialize the root element
	ui->root = &ui->elements[0];
	ui->root->parent = NULL;
	ui->root->id = ui_hash(5381, ":root");
	ui->root->next = ui->root->prev = NULL;
	ui->root->head = ui->root->tail = NULL;
	ui->root->flags = 0;
	ui->root->size[0] = ui_px(ctx->width);
	ui->root->size[1] = ui_px(ctx->height);
	ui->root->rect = rect_init(v2(0, 0), v2(ctx->width, ctx->height));
	ui->root->clipped_rect = ui->root->rect;

	ui->element_count = 1;
	ui->current_parent = ui->root;
	ui->ctx = ctx;

	struct ui_element *main = ui_push_element(UI_DRAW_BACKGROUND, ":main");
	main->size[0] = ui_percent(1, 1);
	main->size[1] = ui_percent(1, 1);

	// NOTE: prebuild the drop downs
	u32 flags = UI_FLOAT_X | UI_FLOAT_Y | UI_DRAW_BACKGROUND;
	ui->drop_down_count = 0;
	for (u32 i = 0; i < 8; i++) {
		ui->drop_downs[i] = ui_push_elementf(flags, ":drop-down[%d]", i);
	}

	// NOTE: prebuild the tooltip container
	struct ui_element *tooltip = ui_push_element(flags, ":tooltip");
	ui->tooltip = tooltip;

	ui->current_parent = main;
}

static struct ui_element *
ui_preorder_next(struct ui_element *element)
{
	if (element->head) {
		element = element->head;
	} else {
		while (element && !element->next) {
			element = element->parent;
		}

		if (element) {
			element = element->next;
		}
	}

	return element;
}

static struct ui_element *
ui_postorder_begin(struct ui_element *root)
{
	struct ui_element *element = root;
	while (element->head) {
		element = element->head;
	}

	return element;
}

static struct ui_element *
ui_postorder_next(struct ui_element *element)
{
	if (!element->next) {
		element = element->parent;
	} else {
		element = element->next;

		while (element->head) {
			element = element->head;
		}
	}

	return element;
}

static f32
ui_children_size_sum(struct ui_element *element, u32 axis)
{
	assert(axis < 2);

	f32 result = 0.f;
	for (struct ui_element *child = element->head; child; child = child->next) {
		struct rect rect = child->rect;
		result += rect.max.e[axis] - rect.min.e[axis];
	}

	return result;
}

static void
ui_end_frame(struct ui_context *_ui)
{
	assert(ui == _ui);

	struct ui_element *root = ui->root;

	// NOTE: the first two phases only set the maximum for each rect and leave
	// the minimum as zero.

	/*
	 * NOTE: calculate upwards dependent and px/em sizes
	 */
	for (struct ui_element *element = root->head;
			element; element = ui_preorder_next(element)) {
		element->rect.max = v2(0, 0);
		if (!(element->flags & UI_FLOAT_X)) {
			element->rect.min.x = 0;
		}

		if (!(element->flags & UI_FLOAT_Y)) {
			element->rect.min.y = 0;
		}

		for (u32 i = 0; i < 2; i++) {
			f32 value = element->size[i].value;
			f32 parent_size = element->parent->rect.max.e[i];
			switch (element->size[i].unit) {
			case UI_PERCENT:
				element->rect.max.e[i] = parent_size * value;
				break;
			case UI_PX:
				element->rect.max.e[i] = value;
				break;
			case UI_EM:
				element->rect.max.e[i] = value * ui_line_height;
				break;
			}
		}
	}

	/*
	 * NOTE: calculate downwards dependent sizes
	 */
	for (struct ui_element *element = ui_postorder_begin(root);
			element; element = ui_postorder_next(element)) {
		for (u32 i = 0; i < 2; i++) {
			if (element->size[i].unit == UI_AUTO) {
				f32 sum = ui_children_size_sum(element, i);

				char buffer[32] = {0};
				char *text = NULL;
				if (element->flags & UI_DRAW_TEXT) {
					text = element->text;
				} else if (element->flags & UI_DRAW_NUMBER) {
					snprintf(buffer, sizeof(buffer), "%g", element->number);
					text = buffer;
				}

				if (text) {
					struct rect text_bounds = render_text(NULL, v2(0, 0),
						rect_inf(), text, ui->font, v4(1, 1, 1, 1));
					f32 size = text_bounds.max.e[i] - text_bounds.min.e[i];
					sum += size + 10;
				}

				element->rect.max.e[i] = sum;
			}
		}
	}

	/*
	 * NOTE: solve any violations
	 */
	for (struct ui_element *element = root->head;
			element; element = ui_preorder_next(element)) {
		for (u32 i = 0; i < 2; i++) {
			f32 sum = ui_children_size_sum(element, i);
			f32 size = element->rect.max.e[i];
			if (sum > size) {
				for (struct ui_element *child = element->head;
						child; child = child->next) {
					struct rect *rect = &child->rect;
					f32 strictness = child->size[i].strictness;
					f32 ratio = (rect->max.e[i] - rect->min.e[i]) * strictness;
					rect->max.e[i] *= 1;
				}
			}
		}
	}

	/*
	 * NOTE: compute the positions
	 */
	f32 x = 0;
	f32 y = ui->ctx->height;
	for (struct ui_element *element = root->head;
			element; element = ui_preorder_next(element)) {
		struct ui_element *prev = element->prev;
		struct ui_element *parent = element->parent;

		// TODO: how to handle position when the previous element is floating?
		if (prev) {
			if (parent->flags & UI_LAYOUT_X) {
				x = prev->rect.max.x;
				y = prev->rect.max.y;
			} else {
				x = prev->rect.min.x;
				y = prev->rect.min.y;
			}
		} else {
			x = parent->rect.min.x;
			y = parent->rect.max.y;
		}

		f32 height = element->rect.max.y;

		if (element->flags & UI_FLOAT_X) {
			x = element->rect.min.x;
			element->rect.min.x = 0;
		}

		if (element->flags & UI_FLOAT_Y) {
			y = element->rect.min.y + height;
			element->rect.min.y = 0;
		}

		// TODO: should we apply scrolling to floating elements?
		x += parent->scroll.x;
		y += parent->scroll.y;

		rect_move(&element->rect, x, y - height);
		element->clipped_rect = rect_intersect(element->rect, parent->clipped_rect);
	}

	/*
	 * NOTE: main ui logic and rendering
	 */
	u32 next_hot = 0;
	u32 next_hot_scroll = 0;
	for (struct ui_element *element = root->head;
			element; element = ui_preorder_next(element)) {
		if (rect_is_empty(element->clipped_rect)) {
			continue;
		}

		u32 id = element->id;
		if (element->flags & UI_SELECTABLE) {
			if (ui->active == id) {
				if (was_released(ui->input.mouse.buttons[1])) {
					ui->active = 0;
				}
			} else if (ui->hot == id) {
				if (was_pressed(ui->input.mouse.buttons[1])) {
					ui->active = id;
				}
			}

			if (rect_contains(element->clipped_rect, ui->input.mouse.pos)) {
				if (!ui->active) {
					next_hot = id;
				}
			}
		}

		if (element->flags & (UI_SCROLL_X | UI_SCROLL_Y)) {
			if (rect_contains(element->clipped_rect, ui->input.mouse.pos)) {
				next_hot_scroll = id;
			}
		}
	}

	ui->hot = next_hot;
	ui->hot_scroll = next_hot_scroll;

	for (struct ui_element *element = root->head;
			element; element = ui_preorder_next(element)) {
		if (rect_is_empty(element->clipped_rect)) {
			continue;
		}

		bool is_hot_scroll = ui->hot_scroll == element->id;
		bool is_hot = ui->hot == element->id;
		bool is_active = ui->active == element->id;

		// NOTE: update the scroll
		v2 children_size = v2(0, 0);
		if (element->flags & (UI_SCROLL_X | UI_SCROLL_Y)) {
			struct ui_element *child = element->head;
			if (child) {
				struct rect children_bounds = child->rect;
				do {
					children_bounds = rect_union(children_bounds, child->rect);
					child = child->next;
				} while (child);

				children_size = rect_size(children_bounds);
			}
		}

		v2 element_size = rect_size(element->rect);
		v2 scroll = element->scroll;
		if (is_hot_scroll && (element->flags & UI_SCROLL_X)) {
			f32 max_scroll = MAX(0, children_size.x - element_size.x);
			scroll.x += 16.f * ui->input.mouse.scroll.x;
			scroll.x = CLAMP(scroll.x, 0, max_scroll);
		}

		if (is_hot_scroll && (element->flags & UI_SCROLL_Y)) {
			f32 max_scroll = MAX(0, children_size.y - element_size.y);
			scroll.y -= 16.f * ui->input.mouse.scroll.y;
			scroll.y = CLAMP(scroll.y, 0, max_scroll);
		}

		element->scroll = scroll;

		// NOTE: draw the background
		if (element->flags & (UI_DRAW_BACKGROUND | UI_DRAW_BACKGROUND2)) {
			v4 bg = v4(0.1, 0.1, 0.1, 1);
			if (element->flags & UI_DRAW_BACKGROUND2) {
				bg = v4(0.2, 0.2, 0.2, 1);
			}

			if (element->flags & UI_SELECTABLE) {
				if (is_active) {
					bg = v4(0.3, 0.3, 0.3, 1);
				} else if (is_hot) {
					bg = v4(0.2, 0.2, 0.2, 1);
				}
			}

			render_rect(ui->ctx, element->clipped_rect, bg);
		}

		// NOTE: draw text or the number
		char buffer[32] = {0};
		char *text = NULL;
		if (element->flags & UI_DRAW_TEXT) {
			assert(element->text);
			text = element->text;
		} else if (element->flags & UI_DRAW_NUMBER) {
			text = buffer;
			snprintf(buffer, sizeof(buffer), "%g", element->number);
		}

		if (text) {
			v4 color = v4(1, 1, 1, 1);
			struct rect rect = element->rect;
			struct rect text_bounds = render_text(NULL,
				rect.min, rect_inf(), text, ui->font, color);
			f32 text_width = text_bounds.max.x - text_bounds.min.x;

			v2 text_pos;
			text_pos.y = floor(rect.max.y - ui_line_height + 5);
			if (element->flags & UI_CENTER_TEXT) {
				text_pos.x = floor(0.5f * (rect.min.x + rect.max.x - text_width));
			} else {
				text_pos.x = rect.min.x + 5;
			}

			render_text(ui->ctx, text_pos, element->clipped_rect, text, ui->font, color);
		}
	}
}

static void
ui_end(void)
{
	assert(ui->current_parent != ui->root);
	if (ui->current_parent != ui->root) {
		ui->current_parent = ui->current_parent->parent;
	}
}

static void
ui_spacer(struct ui_size width, struct ui_size height)
{
	struct ui_element *element = ui_push_element(0, "");
	element->size[0] = width;
	element->size[1] = height;
}

static void
ui_spacer_x(struct ui_size width)
{
	ui_spacer(width, ui_px(1));
}

static void
ui_spacer_y(struct ui_size height)
{
	ui_spacer(ui_px(1), height);
}

static bool
ui_button(char *text)
{
	u32 flags = UI_SELECTABLE | UI_DRAW_TEXT | UI_DRAW_BACKGROUND | UI_CENTER_TEXT;
	struct ui_element *element = ui_push_element(flags, text);
	element->size[0] = ui_percent(1.0f, 0);
	element->size[1] = ui_em(1.0);
	element->text = text;

	return was_released(ui->input.mouse.buttons[1]) && ui->active == element->id;
}

static void
ui_label(char *text)
{
	struct ui_element *element = ui_push_element(UI_DRAW_TEXT, text);
	element->text = text;
	element->size[1] = ui_em(1.0);
}

static void
ui_text_box(char *text)
{
	u32 flags = UI_DRAW_BACKGROUND | UI_SELECTABLE | UI_SCROLL_X | UI_SCROLL_Y;
	struct ui_element *container = ui_push_container(flags, text);
	container->size[0] = ui_percent(1.0f, 0.5f);

	u32 content_flags = UI_DRAW_TEXT | UI_DRAW_BACKGROUND;
	struct ui_element *content = ui_push_element(content_flags, "content");
	content->text = text;

	ui_end();
}

static void
ui_slider(char *label, f32 *value, f32 min, f32 max)
{
	assert(min < max);
	assert(min <= *value && *value <= max);

	// add the base slider
	u32 flags = UI_SELECTABLE | UI_DRAW_BACKGROUND;
	struct ui_element *element = ui_push_container(flags, label);
	element->size[1] = ui_em(1.0);

	// add the bar
	u32 bar_flags = UI_FLOAT_Y | UI_DRAW_BACKGROUND;
	struct ui_element *bar = ui_push_element(bar_flags, "bar");
	bar->size[0] = ui_percent(1.0f * (*value - min) / (max - min), 1.0f);
	bar->size[1] = ui_percent(1.0f, 1.0f);

	// add the text
	u32 text_flags = UI_FLOAT_Y | UI_DRAW_NUMBER | UI_CENTER_TEXT;
	struct ui_element *text = ui_push_element(text_flags, "text");
	text->number = *value;
	text->size[0] = text->size[1] = ui_percent(1, 1);

	if (ui->active == element->id) {
		v2 size = rect_size(element->rect);
		v2 mouse_pos = ui->input.mouse.pos;
		v2 mouse_offset = sub(mouse_pos, element->rect.min);
		f32 t = CLAMP01(mouse_offset.x / size.x);

		*value = lerp(min, max, t);
	} else if (ui->hot == element->id) {
		f32 t = ui->input.mouse.scroll.y;
		*value = CLAMP(*value + 0.05f * t / (max - min), min, max);
	}

	ui_end();
}

static void
ui_row_begin(void)
{
	u32 flags = UI_LAYOUT_X;
	struct ui_element *element = ui_push_container(flags, "");
	element->size[0] = ui_percent(1, 0);
	element->size[1] = ui_em(1.0f);
}

static void
ui_menu_bar_begin(void)
{
	u32 flags = UI_LAYOUT_X | UI_DRAW_BACKGROUND;
	struct ui_element *element = ui_push_container(flags, "");
	element->size[0] = ui_percent(1, 1);
	element->size[1] = ui_em(1.0f);
}

static void
ui_drop_down_begin(void)
{
	struct ui_element *target = ui->current_parent->tail;
	struct ui_element *drop_down = ui->drop_downs[ui->drop_down_count];
	v2 size = rect_size(drop_down->rect);
	if (ui->current_parent->flags & UI_LAYOUT_X) {
		drop_down->rect.min.x = target->rect.min.x;
		drop_down->rect.min.y = target->rect.min.y - size.y;
	} else {
		drop_down->rect.min.x = target->rect.max.x;
		drop_down->rect.min.y = target->rect.min.y - size.y + ui_line_height;
	}

	// NOTE: the tooltip stores the last parent, so ending a tooltip returns
	// the original parent
	ui->drop_downs[ui->drop_down_count++] = ui->current_parent;
	ui->current_parent = drop_down;
}

static void
ui_drop_down_end(void)
{
	struct ui_element *drop_down = ui->drop_downs[--ui->drop_down_count];
	ui->drop_downs[ui->drop_down_count] = ui->current_parent;
	ui->current_parent = drop_down;
}

static void
ui_tooltip_begin(void)
{
	assert(ui->current_parent->tail);

	struct ui_element *target = ui->current_parent->tail;
	struct ui_element *tooltip = ui->tooltip;
	v2 size = rect_size(tooltip->rect);
	tooltip->rect.min.x = target->rect.min.x;
	tooltip->rect.min.y = target->rect.min.y - size.y;

	// NOTE: the tooltip stores the last parent, so ending a tooltip returns
	// the original parent
	ui->tooltip = ui->current_parent;
	ui->current_parent = tooltip;
}

static void
ui_tooltip_end(void)
{
	struct ui_element *tooltip = ui->tooltip;
	ui->tooltip = ui->current_parent;
	ui->current_parent = tooltip;
}

static void
ui_separator(void)
{
	struct ui_element *element = ui_push_element(UI_DRAW_BACKGROUND2, "");
	if (element->parent->flags & UI_LAYOUT_X) {
		element->size[0] = ui_px(1);
		element->size[1] = ui_percent(1, 1);
	} else {
		element->size[0] = ui_percent(1, 1);
		element->size[1] = ui_px(1);
	}
}
