enum token_type {
	PRAM_EOF,
	PRAM_PLUS,
	PRAM_STAR,
	PRAM_COLON,
	PRAM_LPAREN,
	PRAM_RPAREN,
	PRAM_NUMBER,
	PRAM_IDENTIFIER,
	// NOTE: Only instructions should follow.
	PRAM_NOP,
	PRAM_GET,
	PRAM_SET,
	PRAM_MOV,
	PRAM_STR,
	PRAM_MOV_STAR,
	PRAM_STR_STAR,
	PRAM_ADD,
	PRAM_SUB,
	PRAM_DIV,
	PRAM_JMP,
	PRAM_JIZ,
	PRAM_JIP,

	PRAM_FIRST_INSTRUCTION = PRAM_NOP,
};

struct token {
	enum token_type type;
	u32 start;
	u32 length;

	union {
		char *string;
		i32 number;
	};
};

struct parser {
	struct token token;
	struct buffer buffer;

	bool is_initialized;
	u32 error;
	u32 machine_count;
};

struct pram_memory {
	i32 *inputs;
	i32 *registers;

	u32 input_count;
	u32 register_count;
};

enum pram_expression_type {
	PRAM_EXPR_NONE,
	PRAM_EXPR_MACHINE_COUNT,
	PRAM_EXPR_MACHINE_INDEX,
	PRAM_EXPR_VARIABLE,
	PRAM_EXPR_NUMBER,
	PRAM_EXPR_ADD,
	PRAM_EXPR_MUL,
};

struct pram_expression {
	enum pram_expression_type type;
	union {
		struct {
			struct pram_expression *lhs;
			struct pram_expression *rhs;
		};

		char *variable;
		i32 number;
	};
};

struct pram_instruction {
	u32 opcode;
	u32 line;
	struct pram_expression arg;
};

struct pram_program {
	struct pram_instruction *instructions;
	u32 *counters;

	u32 instruction_count;
	u32 instruction_size;
	u32 machine_count;
};

static const char *token_name[] = {
	[PRAM_EOF] = "EOF",
	[PRAM_PLUS] = "+",
	[PRAM_STAR] = "*",
	[PRAM_COLON] = ":",
	[PRAM_NUMBER] = "number",
	[PRAM_IDENTIFIER] = "identifier",
	[PRAM_NOP] = "nop",
	[PRAM_GET] = "get",
	[PRAM_SET] = "set",
	[PRAM_MOV] = "mov",
	[PRAM_STR] = "str",
	[PRAM_MOV_STAR] = "mov*",
	[PRAM_STR_STAR] = "str*",
	[PRAM_ADD] = "add",
	[PRAM_SUB] = "sub",
	[PRAM_DIV] = "div",
	[PRAM_JMP] = "jmp",
	[PRAM_JIZ] = "jiz",
	[PRAM_JIP] = "jip",
};

static bool parse(struct parser *parser, struct pram_program *program);
static i32 expression_eval(struct pram_expression *expression,
	u32 machine_index, u32 machine_count);
static bool program_step(struct pram_program *program, struct pram_memory *memory);
static void program_finish(struct pram_program *program);
static void memory_init(struct pram_memory *memory, const char *input_path);
static void memory_finish(struct pram_memory *memory);
