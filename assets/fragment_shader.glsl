in v4 frag_color;
in v2 frag_texcoord;
in f32 frag_texture_index;

uniform sampler2D color_textures[16];

out v4 out_color;

void
main(void)
{
	v4 color = frag_color;

	switch (i32(frag_texture_index)) {
	default: color *= texture(color_textures[ 0], frag_texcoord); break;
	case  1: color *= texture(color_textures[ 1], frag_texcoord); break;
	case  2: color *= texture(color_textures[ 2], frag_texcoord); break;
	case  3: color *= texture(color_textures[ 3], frag_texcoord); break;
	case  4: color *= texture(color_textures[ 4], frag_texcoord); break;
	case  5: color *= texture(color_textures[ 5], frag_texcoord); break;
	case  6: color *= texture(color_textures[ 6], frag_texcoord); break;
	case  7: color *= texture(color_textures[ 7], frag_texcoord); break;
	case  8: color *= texture(color_textures[ 8], frag_texcoord); break;
	case  9: color *= texture(color_textures[ 9], frag_texcoord); break;
	case 10: color *= texture(color_textures[10], frag_texcoord); break;
	case 11: color *= texture(color_textures[11], frag_texcoord); break;
	case 12: color *= texture(color_textures[12], frag_texcoord); break;
	case 13: color *= texture(color_textures[13], frag_texcoord); break;
	case 14: color *= texture(color_textures[14], frag_texcoord); break;
	case 15: color *= texture(color_textures[15], frag_texcoord); break;
	}

	if (color.a < 0.1) {
		discard;
	}

	out_color = color;
}
