layout (location = 0) in v2 vert_pos;
layout (location = 1) in v2 vert_texcoord;
layout (location = 2) in v4 vert_color;
layout (location = 3) in f32 vert_texture_index;

uniform v2 viewport;

out v4 frag_color;
out v2 frag_texcoord;
out f32 frag_texture_index;

void
main(void)
{
	frag_color = vert_color;
	frag_texcoord = vert_texcoord;
	frag_texture_index = vert_texture_index;
	gl_Position = v4(2.0 * vert_pos / viewport - v2(1.0), 0.0, 1.0);
}
