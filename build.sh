#!/bin/sh

set -e

mkdir -p build
cc -g -std=c11 -Wall -D_POSIX_C_SOURCE=200809L -Iglad/include -I. \
	-Lglfw/build/src/ -o build/pram pram/pram.c glad/src/glad.c -lglfw3 -lm
